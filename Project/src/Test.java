import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Test {
	public static void main(String[] args) {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  
		   LocalDateTime now = LocalDateTime.now(); 
		String updateDate = now.format(dtf);
		
		System.out.println(updateDate);

	}
}
