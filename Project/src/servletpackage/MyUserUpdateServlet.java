package servletpackage;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import daopackage.Userdao;
import modelbeanspackage.User;

/**
 * Servlet implementation class MyUserUpdateServlet
 */
@WebServlet("/MyUserUpdateServlet")
public class MyUserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public MyUserUpdateServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");

		String id = request.getParameter("id");

		Userdao userdao = new Userdao();
		User user = userdao.detailsInfo(id);

		request.setAttribute("id", user.getId());
		request.setAttribute("loginId", user.getLoginId());
		request.setAttribute("name", user.getName());
		request.setAttribute("birthDate", user.getBirthDate());

		RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/userupdate.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");

		String id = request.getParameter("id");

		Userdao userdao = new Userdao();
		User user = userdao.detailsInfo(id);

		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String passwordconfirm = request.getParameter("passwordconfirm");
		String name = request.getParameter("name");
		String birthDate = request.getParameter("birthDate");
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();
		String updateDate = now.format(dtf);

		System.out.println(updateDate);

		if (!password.equals(passwordconfirm)) {

			request.setAttribute("errormessage", "パスワードの内容が正しくありません");
			request.setAttribute("id", id);
			request.setAttribute("loginId", user.getLoginId());
			request.setAttribute("name", name);
			request.setAttribute("birthDate", birthDate);

			RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/userupdate.jsp");
			dispatcher.forward(request, response);
			return;
		}

		if (password.equals("") && passwordconfirm.equals("")) {
			userdao.noPasswordUpdateInfo(name, birthDate, updateDate, id);
		} else {
			userdao.updateInfo(password, name, birthDate, updateDate, id);
		}

		response.sendRedirect("MyUserListServlet");

	}

}
