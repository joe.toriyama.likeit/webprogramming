package servletpackage;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import daopackage.Userdao;
import modelbeanspackage.User;

/**
 * Servlet implementation class MyUserCreateServlet
 */
@WebServlet("/MyUserCreateServlet")
public class MyUserCreateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public MyUserCreateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");

		RequestDispatcher dispatcher =request.getRequestDispatcher("WEB-INF/jsp/usercreate.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");

		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String name = request.getParameter("name");
		String birthDate = request.getParameter("birthDate");

		Userdao userdao = new Userdao();

		User user = userdao.findByLoginId(loginId);

		String passwordconfirm = request.getParameter("passwordconfirm");

		if(! password.equals(passwordconfirm)) {
			request.setAttribute("errormessage", "パスワードの内容が正しくありません");
			request.setAttribute("loginId", loginId);
			request.setAttribute("name", name);
			request.setAttribute("birthDate", birthDate);
			RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/usercreate.jsp");
			dispatcher.forward(request, response);
			return;
		}

		if(loginId.equals("") || password.equals("") || name.equals("") || birthDate.equals("") ){
			request.setAttribute("errormessage", "ログインID、パスワード、ユーザー名もしくは生年月日が空欄です");
			request.setAttribute("loginId", loginId);
			request.setAttribute("name", name);
			request.setAttribute("birthDate", birthDate);
			RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/usercreate.jsp");
			dispatcher.forward(request, response);
			return;
		}

		 if(user != null) {
			request.setAttribute("errormessage", "そのログインIDはもう存在します");
			request.setAttribute("loginId", loginId);
			request.setAttribute("name", name);
			request.setAttribute("birthDate", birthDate);
			RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/usercreate.jsp");
			dispatcher.forward(request, response);
			return;
		}

		 userdao.createUser(loginId, name, birthDate, password);

		response.sendRedirect("MyUserListServlet");
	}

}

//
