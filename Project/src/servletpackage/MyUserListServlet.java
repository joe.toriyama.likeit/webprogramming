package servletpackage;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import daopackage.Userdao;
import modelbeanspackage.User;

/**
 * Servlet implementation class UserListServlet
 */
@WebServlet("/MyUserListServlet")
public class MyUserListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public MyUserListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");

		Userdao userdao = new Userdao();
		List<User> userList = userdao.findAll();

		request.setAttribute("userList", userList);

		RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/userlist.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");

		String loginId = request.getParameter("loginId");
		String name = request.getParameter("userName");
		String birthDateStart = request.getParameter("date-start");
		String birthDateEnd = request.getParameter("date-end");

		Userdao userdao = new Userdao();
		List<User> userList = userdao.search(loginId, name, birthDateStart, birthDateEnd);

		request.setAttribute("userList", userList);

		RequestDispatcher dispatcher =  request.getRequestDispatcher("WEB-INF/jsp/userlist.jsp");
		dispatcher.forward(request, response);


	}

}
