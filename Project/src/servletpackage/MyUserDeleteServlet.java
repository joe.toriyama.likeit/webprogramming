package servletpackage;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import daopackage.Userdao;
import modelbeanspackage.User;

/**
 * Servlet implementation class MyUserDeleteServlet
 */
@WebServlet("/MyUserDeleteServlet")
public class MyUserDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public MyUserDeleteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");

		String id = request.getParameter("id");

		Userdao userdao = new Userdao();
		User user = userdao.detailsInfo(id);

		request.setAttribute("user2", user);



		RequestDispatcher dispatcher =request.getRequestDispatcher("WEB-INF/jsp/userdelete.jsp");
		dispatcher.forward(request, response);	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		request.setCharacterEncoding("UTF-8");

		String id = request.getParameter("id");
		Userdao userdao = new Userdao();
		userdao.deleteUser(id);


		response.sendRedirect("MyUserListServlet");

	}

}



