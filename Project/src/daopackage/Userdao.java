package daopackage;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import modelbeanspackage.User;

public class Userdao {

	public String md5(String password) {


		String source = password;

		Charset charset = StandardCharsets.UTF_8;
		String algorithm = "MD5";
		byte[] bytes = null;
		try {
			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		String result = DatatypeConverter.printHexBinary(bytes);

		return result;

	}

public User findByLoginInfo(String loginId, String password) {
	Connection conn = null;
	try {
		String md5password = this.md5(password);
		conn = DBManager.getConnection();
		String sql = "select * from user where login_id = ? and password = ?";
		PreparedStatement pStmt = conn.prepareStatement(sql);
		pStmt.setString(1,  loginId);
		pStmt.setString(2, md5password);
		ResultSet rs = pStmt.executeQuery();

		if (!rs.next()) {
			return null;
		}

		String loginIdData = rs.getString("login_id");
		String nameData = rs.getString("name");
		return new User(loginIdData, nameData);

	} catch (SQLException e) {
        e.printStackTrace();
        return null;
    } finally {
        // データベース切断
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        }
    }
}
public List<User> findAll() {
	Connection conn = null;
	List<User> userList = new ArrayList<User>();

	try {
		conn = DBManager.getConnection();
		String sql = "select * from user where login_id != 'admin' ";
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(sql);



		while(rs.next()) {
			int id = rs.getInt("id");
			String loginId = rs.getString("login_id");
            String name = rs.getString("name");
            Date birthDate = rs.getDate("birth_date");
            String password = rs.getString("password");
            String createDate = rs.getString("create_date");
            String updateDate = rs.getString("update_date");
            User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

            userList.add(user);
		}
	 } catch (SQLException e) {
         e.printStackTrace();
         return null;
     } finally {
         // データベース切断
         if (conn != null) {
             try {
                 conn.close();
             } catch (SQLException e) {
                 e.printStackTrace();
                 return null;
             }
         }
     }
     return userList;
 }

public User detailsInfo(String id) {
	Connection conn = null;
	try {
		conn = DBManager.getConnection();
		String sql = "select * from user where id = ?";
		PreparedStatement pStmt = conn.prepareStatement(sql);
		pStmt.setString(1,  id);

		ResultSet rs = pStmt.executeQuery();

		if (!rs.next()) {
			return null;
		}
		int idData = rs.getInt("id");
		String loginIdData = rs.getString("login_id");
		String nameData = rs.getString("name");
		Date birthDateData = rs.getDate("birth_date");
		String passwordData = rs.getString("password");
		String createDateData = rs.getString("create_date");
		String updateDateData = rs.getString("update_date");

		return new User(idData, loginIdData, nameData, birthDateData, passwordData, createDateData, updateDateData);

	} catch (SQLException e) {
        e.printStackTrace();
        return null;
    } finally {
        // データベース切断
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        }
    }
}

public User deleteUser(String id) {
	Connection conn = null;
	try {
		conn = DBManager.getConnection();
		String sql = "delete from user where id = ?";
		PreparedStatement pStmt = conn.prepareStatement(sql);
		pStmt.setString(1, id);
		pStmt.executeUpdate();



		return null;
	} catch (SQLException e) {
        e.printStackTrace();
        return null;
    } finally {
        // データベース切断
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        }
    }
}

public User updateInfo(String password, String name, String birthDate, String updateDate, String id) {
	Connection conn = null;
	try {

		String md5password = this.md5(password);
		conn = DBManager.getConnection();
		String sql = "update user set \r\n" +
				"password = ?,\r\n" +
				"name = ?,\r\n" +
				"birth_date = ?,\r\n" +
				"update_date = ?\r\n" +
				"where id = ?;";
		PreparedStatement pStmt = conn.prepareStatement(sql);
		pStmt.setString(1,  md5password);
		pStmt.setString(2,  name);
		pStmt.setString(3,  birthDate);
		pStmt.setString(4, updateDate);
		pStmt.setString(5,  id);

		pStmt.executeUpdate();

		return null;

	} catch (SQLException e) {
        e.printStackTrace();
        return null;
    } finally {
        // データベース切断
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        }
    }
}


public User createUser(String loginId, String name, String birthDate, String password) {
	Connection conn = null;
	try {

		String md5password = this.md5(password);
		conn = DBManager.getConnection();
		String sql = "insert into user (login_id, name, birth_date, password, create_date, update_date)\r\n" +
				"values (?, ?, ?, ?, NOW(), NOW())";



		PreparedStatement pStmt = conn.prepareStatement(sql);
		pStmt.setString(1, loginId);
		pStmt.setString(2, name);
		pStmt.setString(3, birthDate);
		pStmt.setString(4, md5password);


		pStmt.executeUpdate();

		return null;
	} catch (SQLException e) {
        e.printStackTrace();
        return null;
    } finally {
        // データベース切断
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        }
    }
}
public User findByLoginId(String loginId) {
	Connection conn = null;
	try {
		conn = DBManager.getConnection();
		String sql = "select * from user where login_id = ?";
		PreparedStatement pStmt = conn.prepareStatement(sql);
		pStmt.setString(1,  loginId);

		ResultSet rs = pStmt.executeQuery();

		if (!rs.next()) {
			return null;
		}

		String loginIdData = rs.getString("login_id");

		return new User(loginIdData);

	} catch (SQLException e) {
        e.printStackTrace();
        return null;
    } finally {
        // データベース切断
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        }
    }
}

public List<User> search(String loginId, String name, String birthDateStart, String birthDateEnd) {
	Connection conn = null;
	List<User> userList = new ArrayList<User>();
	try {
		conn = DBManager.getConnection();
		//String sql = "select * from user where login_id like '%"+ loginId +"%' and name like '%"+ name +"%' and birth_date between '"+ birthDateStart +"' and '"+ birthDateEnd +"'";
		String sql = "select * from user where login_id  != 'admin'";

		if(!loginId.equals("")) {
			sql += " and login_id = '"+loginId+"' ";
		}

		if(!name.equals("")) {
			sql += " and name like '%"+name+"%' ";
		}

		if(!birthDateStart.equals("")) {
			sql += " and birth_date >= '"+birthDateStart+"' ";
		}

		if(!birthDateEnd.equals("")) {
			sql += " and birth_date <= '"+birthDateEnd+"' ";
		}
		System.out.println(sql);
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(sql);

		while(rs.next()) {
		int idData = rs.getInt("id");
		String loginIdData = rs.getString("login_id");
		String nameData = rs.getString("name");
		Date birthDateData = rs.getDate("birth_date");
		String passwordData = rs.getString("password");
		String createDateData = rs.getString("create_date");
		String updateDateData = rs.getString("update_date");

		User user = new User(idData, loginIdData, nameData, birthDateData, passwordData, createDateData, updateDateData);

		userList.add(user);
		}

	} catch (SQLException e) {
        e.printStackTrace();
        return null;
    } finally {
        // データベース切断
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        }
    }
 return userList;


}

public User noPasswordUpdateInfo(String name, String birthDate, String updateDate, String id) {
	Connection conn = null;
	try {

		conn = DBManager.getConnection();
		String sql = "update user set \r\n" +
				"name = ?,\r\n" +
				"birth_date = ?,\r\n" +
				"update_date = ?\r\n" +
				"where id = ?";
		PreparedStatement pStmt = conn.prepareStatement(sql);
		pStmt.setString(1,  name);
		pStmt.setString(2,  birthDate);
		pStmt.setString(3,  updateDate);
		pStmt.setString(4,  id);

		pStmt.executeUpdate();

		return null;

	} catch (SQLException e) {
        e.printStackTrace();
        return null;
    } finally {
        // データベース切断
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        }
    }

}

}