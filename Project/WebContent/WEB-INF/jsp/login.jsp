<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ログイン</title>
    <!-- BootstrapのCSS読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- オリジナルCSS読み込み -->
    <link href="CSS/logincss.css" rel="stylesheet">

    <style>
        img {
            display: block;
            margin-left: auto;
            margin-right: auto;
        }

    </style>

</head>

<body style="background-color:powderblue;">


	<div>
	<c:if test="${errMsg != null}" >
	<div class="alert alert-danger" role="alert">  ${errMsg}
	</div>
	</c:if>
	</div>

    <!-- body -->
    <form class="form-signin" action="MyLoginServlet" method="post"
           >
         <div class="center">
        <img src="img/apple-png-apple-png-2040.png" alt="applepngfile" class="center" width="260" height="300">

    </div>
        <div class="text-center mb-4">
            <h1 class="h3 mb-3 font-weight-normal">ログイン</h1>
        </div>

        <label for="inputLoginId">ログインID</label>

        <div class="form-label-group" class="col-sm-6">
            <input type="text" name="loginId" id="inputLoginId" class="form-control" placeholder="ログインIDを入力してください" autofocus>
        </div>
        <label for="inputPassword">パスワード
        </label>

        <div class="form-label-group" class="col-sm-6">
            <input type="password" name="password" id="inputPassword" class="form-control" placeholder="パスワードを入力してください">
        </div>

        <button class="btn btn-lg btn-primary btn-block" type="submit" class="col-sm-6">ログイン</button>
    </form>


</body>

</html>
