<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ユーザ更新画面</title>
<!-- BootstrapのCSS読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<!-- オリジナルCSS読み込み -->
<link href="CSS/common.css" rel="stylesheet">

</head>

<body style="background-color: powderblue;">

	<!-- header -->
	<header>
		<nav class="navbar navbar-dark bg-dark navbar-expand  flex-md-row">
			<ul class="navbar-nav navbar-dark bg-dark flex-row mr-auto">
				<li class="nav-item active"><a class="nav-link"
					href="MyUserListServlet">ユーザ管理システム</a></li>
			</ul>
			<ul class="navbar-nav flex-row">
				<li class="nav-item"><a class="nav-link" href="#">${userInfo.name}
						さん</a></li>
				<li class="nav-item"><a class="btn btn-primary"
					href="MyLogoutServlet">ログアウト</a></li>
			</ul>
		</nav>
	</header>
	<!-- /header -->

	<!-- body -->
	<div class="container">

		<div>
			<c:if test="${errormessage != null}">
				<div class="alert alert-danger" role="alert">${errormessage}</div>
			</c:if>
		</div>
		<!-- エラーメッセージのサンプル(エラーがある場合のみ表示) -->
		<!--  <div class="alert alert-danger" role="alert">エラーがある場合のメッセージサンプル</div>-->


		<form method="post" action="MyUserUpdateServlet" class="form-horizontal">
			<div class="form-group row">
				<label for="loginId" class="col-sm-2 col-form-label">ログインID</label>
				<div class="col-sm-10">
					<p class="form-control-plaintext">${loginId}</p>
					<input type="hidden" value="${loginId}" name="loginId" >
				</div>
			</div>

			<div class="form-group row">
				<label for="password" class="col-sm-2 col-form-label">パスワード</label>
				<div class="col-sm-10">
					<input name="password" type="password" class="form-control" id="password">
				</div>
			</div>

			<div class="form-group row">
				<label for="passwordConf" class="col-sm-2 col-form-label">パスワード(確認)</label>
				<div class="col-sm-10">
					<input name="passwordconfirm" type="password" class="form-control" id="passwordConf">
				</div>
			</div>

			<div class="form-group row">
				<label for="userName" class="col-sm-2 col-form-label">ユーザ名</label>
				<div class="col-sm-10">
					<input name="name" type="text" class="form-control" id="userName"
						value="${name}">
				</div>
			</div>

			<div class="form-group row">
				<label for="birthDate" class="col-sm-2 col-form-label">生年月日</label>
				<div class="col-sm-10">
					<input name="birthDate" type="date" class="form-control" id="birthDate"
						value="${birthDate}">
				</div>
			</div>

			<div class="submit-button-area">
			<input type="hidden" value="${id}" name="id">
				<button type="submit" value="検索" class="btn btn-primary">更新</button>
			</div>

			<div class="col-xs-4">
				<a href="MyUserListServlet">戻る</a>
			</div>

		</form>


	</div>






</body>

</html>