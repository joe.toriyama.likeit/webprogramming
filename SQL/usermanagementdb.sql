﻿drop database usermanagement;
create database usermanagement default character set utf8;

use usermanagement;

create table user(
id serial primary key unique not null auto_increment,
login_id varchar(255) unique not null,
name varchar(255) not null,
birth_date date not null,
password varchar(255) not null,
create_date datetime not null,
update_date datetime not null);

insert into user values(1, 'admin','管理者', '2000-01-01', 'password', '2020-01-01', '2020-01-02');


/*
as ID 
as ログインID
as 名前
as 生年月日
as パスワード
as 作成日時
as 更新日時
*/
